<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Register
Route::get('/view', 'Auth\RegisterController@index');
Route::post('/register', 'Auth\RegisterController@store');

//login
Route::post('/login', 'Auth\LoginController@check');

//post
Route::get('/post/view', 'Content\PostController@index');
Route::post('/post/store', 'Content\PostController@store');
Route::post('/post/update', 'Content\PostController@update');
Route::post('/post/delete', 'Content\PostController@delete');
Route::post('/post/byId', 'Content\PostController@byId');
Route::post('/post/byUser', 'Content\PostController@byUser');
Route::post('/post/byCategory', 'Content\PostController@byCategory');

Route::prefix('admin')->group(function() {
    Route::post('/login', 'Auth\AdminController@login');
    Route::post('/registerAdmin', 'Auth\AdminController@register');

    Route::get('/user/{id}', 'User\UserController@userDetail');
    Route::get('/user-list', 'User\UserController@userList');
    Route::post('/user-register', 'Admin\UserController@registerUser');
    Route::post('/user-update', 'Admin\UserController@updateUser');
    Route::post('/user-updates', 'Admin\UserController@updateUsers');
    Route::post('/user-delete', 'Admin\UserController@deleteUser');
});
