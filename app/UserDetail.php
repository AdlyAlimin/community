<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = [
        'email',
        'fullname',
        'student_id',
        'phone_number'
    ];

    public function userDetail()
    {
        return $this->belongsTo('App\User');
    }
}
