<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    protected $fillable = [
        'username',
        'email',
        'password',
        'ic_number',
        'role',
    ];

    public function user()
    {
        return $this->hasOne('App\UserDetail');
    }
}
