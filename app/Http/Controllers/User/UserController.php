<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Exception;

class UserController extends Controller
{
    public function userList() {

        try{
            $userList = DB::table('users')
                            ->select('id', 'email', 'username')
                            ->get();

            return response()->json(['results' => $userList], 200);
        }catch(Exception $e){
            return response()->json(['results' => $e], 400);
        }

    }

    public function userDetail($id) {

        try {
            $userDetail = DB::table('users')
                            ->where('id', $id)
                            ->first();

            return response()->json(['results' => $userDetail], 200);
        } catch(Exception $e) {
            return response()->json(['results' => $e], 400);
        }
    }

    public function adduser(Request $request) {

        try{
            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'username' => 'required',
                'password' => 'required',
                'role' => 'required',
                'fullname' => 'required',
                'student_id' => 'required',
            ]);
    
            if($validator->fails()) {
                return response()->json(['message' => 'error'], 400);
            }
    
            $user = new User([
                'email' => $request->email,
                'username' => $request->username,
                'password' => bcrypt($request->password),
                'role' => $request->role,
                'ic_number' => $request->ic_number,
            ]);
    
            $userDetail = new UserDetail([
                'email' => $request->email,
                'fullname' => $request->fullname,
                'student_id' => $request->student_id,
            ]);
    
            $user->save();
            $userDetail->save();
    
            return response()->json(['message' => 'success'], 200);
        }catch(Exception $e){
            return response()->json(['message' => $e], 400);
        }

    }
}
