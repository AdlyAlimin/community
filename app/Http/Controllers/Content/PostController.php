<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Str;

class PostController extends Controller
{
    public function index() {

        try{
            $postList = DB::table('postings')
                            ->get();

            return response()->json(['message' => $postList], 200);
        }catch(Exception $e){
            return response()->json(['message' => $e], 400);
        }

    }

    public function byUser(Request $request) {

        try{
            $postList = DB::table('postings')
                            ->where('email', $request->email)
                            ->get();

            return response()->json(['message' => $postList], 200);
        }catch(Exception $e){
            return response()->json(['message' => $e], 400);
        }

    }

    public function byId(Request $request) {

        try{
            $postDetail = DB::table('postings')
                            ->where('post_id', $request->post_id)
                            ->first();

            $userDetail = DB::table('user_details')
                            ->where('email', $postDetail->email)
                            ->first();

            $postMedia = DB::table('post_medias')
                            ->where('post_id', $request->post_id)
                            ->get();

            return response()->json(['message' => $postDetail, 'postMedia' => $postMedia, 'userDetail' => $userDetail], 200);
        }catch(Excception $e){
            return response()->json(['message' => $e], 400);
        }

    }

    public function byCategory(Request $request) {

        try{

            $postList = DB::table('postings')
                            ->where('category', $request->category)
                            ->get();

            if($postList->isEmpty()){
                return response()->json(null, 400);
            }else{
                return response()->json(['message' => $postList], 200);
            }

        }catch(Exception $e){
            return response()->json(['message' => $e], 400);
        }
    }

    public function store(Request $request) {

        try{

            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'subject' => 'required',
                'description' => 'required',
                'location' => 'required',
                'category' => 'required',
            ]);

            if($validator->fails()) {
                return response()->json(['message' => 'Field has been empty'], 400);
            }

            $post_id = DB::table('postings')
                            ->orderBy('post_id', 'desc')
                            ->first();

            if(is_null($post_id)){
                $post_ids = 'ID0000';
            }else{
                $post_id = Str::substr($post_id -> post_id, 2);
                $post_ids = sprintf('ID%04d', $post_id + 1);
            }

            DB::table('postings')
                ->insert([
                    'post_id' => $post_ids,
                    'email' => $request->email,
                    'subject' => $request->subject,
                    'description' => $request->description,
                    'location' => $request->location,
                    'category' => $request->category,
                ]);

            if($request->hasFile('files')){
                $posting_img = $request->file('files');

                foreach($posting_img as $post_img) {
                    $imgName = md5($post_img->getClientOriginalName()). '.'. $post_img->getClientOriginalExtension();

                    $post_img->move(public_path('posting'), $imgName);

                    DB::table('post_medias')
                        ->insert([
                            'post_id' => $post_ids,
                            'file_name' => $imgName,
                            'file_ext' => $post_img->getClientOriginalExtension(),
                        ]);
                }
            }

            return response()->json(['message' => 'Success'], 200);
        }catch(Exception $e) {
            return response()->json(['message' => $e], 400);
        }
    }

    public function update(Request $request) {

        try{

            DB::table('postings')
                ->where('post_id', $request->post_id)
                ->update([
                    'subject' => $request->subject,
                    'description' => $request->description,
                    'location' => $request->location,
                    'category' => $request->category,
                ]);

            return response()->json(['message' => 'updated'], 200);

        }catch(Exception $e){
            return response()->json(['message' => $e], 400);
        }

    }

    public function delete(Request $request) {

        try{

            DB::table('postings')
                ->where('post_id', $request->post_id)
                ->delete();

            return response()->json(['message' => 'deleted'], 200);

        }catch(Exception $e){
            return response()->json(['message' => $e], 400);
        }

    }
}
