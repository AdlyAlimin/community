<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Hash;

class LoginController extends Controller
{
    public function check(Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['message' => 'Field Required.'], 400);
        }

        $checkLogin = User::where('email', $request->email)
                            ->first();

        if(!$checkLogin instanceof User) return response()->json(['message' => 'Email Not Found or Match'], 400);

        if(!Hash::check($request->password, $checkLogin -> password)) return response()->json(['message' => 'Password Not Match'], 400);

        return response()->json(['message' => 'login success'], 200);

    }

}
