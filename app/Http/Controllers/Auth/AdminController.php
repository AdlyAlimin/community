<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use Validator;
use Hash;

class AdminController extends Controller
{
    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['message' => 'Field Required.'], 400);
        }

        $checkLogin = Admin::where('email', $request->email)
                            ->first();

        if(!$checkLogin instanceof Admin) return response()->json(['message' => 'Email Not Found or Match'], 400);

        if(!Hash::check($request->password, $checkLogin -> password)) return response()->json(['message' => 'Password Not Match'], 400);

        return response()->json(['message' => 'login success'], 200);

    }

    public function register(Request $request) {

        $admin = new Admin;

        $admin->username = $request->username;
        $admin->email = $request->email;
        $admin->role = $request->role;
        $admin->password = bcrypt($request->password);

        $admin->save();
    }
    
}
