<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\UserDetail;
use Validator;
use Exception;

class RegisterController extends Controller
{

    public function store(Request $request) {

        try{
            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'username' => 'required',
                'password' => 'required',
                'role' => 'required',
                'fullname' => 'required',
                'student_id' => 'required',
            ]);
    
            if($validator->fails()) {
                return response()->json(['message' => 'error'], 400);
            }
    
            $user = new User([
                'email' => $request->email,
                'username' => $request->username,
                'password' => bcrypt($request->password),
                'role' => $request->role,
                'ic_number' => $request->ic_number,
            ]);
    
            $userDetail = new UserDetail([
                'email' => $request->email,
                'fullname' => $request->fullname,
                'student_id' => $request->student_id,
            ]);
    
            $user->save();
            $userDetail->save();
    
            return response()->json(['message' => 'success'], 200);
        }catch(Exception $e){
            return response()->json(['message' => $e], 400);
        }
    }

}
