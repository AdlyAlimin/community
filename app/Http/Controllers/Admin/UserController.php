<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;
use Hash;

use App\User;
use App\UserDetail;

class UserController extends Controller
{
    public function registerUser(Request $request){

        try{
            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'username' => 'required',
                'password' => 'required',
                'role' => 'required',
                'fullname' => 'required',
                'student_id' => 'required',
            ]);
    
            if($validator->fails()) {
                return response()->json(['message' => 'error'], 400);
            }
    
            $user = new User([
                'email' => $request->email,
                'username' => $request->username,
                'password' => bcrypt($request->password),
                'role' => $request->role,
                'ic_number' => $request->ic_number,
            ]);
    
            $userDetail = new UserDetail([
                'email' => $request->email,
                'fullname' => $request->fullname,
                'student_id' => $request->student_id,
            ]);
    
            $user->save();
            $userDetail->save();
    
            return response()->json(['message' => 'success'], 200);
        }catch(Exception $e){
            return response()->json(['message' => $e], 400);
        }
    }

   

    public function updateUser(Request $request){

        try{
            $updateUser = DB::table('users')
                            ->where('id', $request->id)
                            ->update([
                                'password' => bcrypt($request->password)
                            ]);

            return response()->json(['results' => 'success'], 200);
        }catch(Exception $e){
            return response()->json(['results' => $e], 400);
        }

    }

    public function updateUsers(Request $request){

        try{
            $updateUser = DB::table('users')
                            ->where('id', $request->id)
                            ->update([
                                'username' => $request->username,
                                'email' => $request->email,
                            ]);

            return response()->json(['results' => 'success'], 200);
        }catch(Exception $e){
            return response()->json(['results' => $e], 400);
        }

    }

    public function deleteUser(Request $request){

        try{
            $deleteUser = DB::table('users')
                            ->where('id', $request->id)
                            ->delete();

            return response()->json(['results' => 'success'], 200);
        }catch(Exception $e){
            return response()->json(['results' => $e], 400);
        }

    }
}
